import java.util.Queue;

public class Consumer extends Thread {
    private final Buffer buffer;

    public Consumer(Buffer _buffer) {
        this.buffer = _buffer;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (buffer) {
                if (buffer.isEmpty()) {
                    try {
                        buffer.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                if (buffer.front() == -1) {
                    return;
                }
                System.out.print("\"" + buffer.poll() + "\", ");
                System.out.println();
                buffer.notifyAll();
            }
        }
    }
}
