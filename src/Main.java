import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class Main extends Thread {
    public static void main(String[] args) throws InterruptedException, IOException {
        Scanner s = new Scanner(System.in);
        PrintStream out = new PrintStream(new FileOutputStream("output.txt"));
        System.setOut(out);

        int number = s.nextInt();
        int bufferSize = s.nextInt();

        Buffer buffer = new Buffer(bufferSize);

        Producer producer;
        Consumer consumer;

        producer = new Producer(buffer, number, bufferSize);
        consumer = new Consumer(buffer);

        long start = System.currentTimeMillis();
        producer.start();
        consumer.start();

        producer.join();
        consumer.join();
        long end = System.currentTimeMillis();

        System.out.println();
        System.out.print(end - start + " ms");
        out.close();
    }
}