import java.util.Queue;

public class Producer extends Thread {
    private final int number;
    private final Buffer buffer;
    private final int buffer_size;

    private final PrimesGenerator gen = new PrimesGenerator();

    public Producer(Buffer _buffer, int _number, int _buffer_size) {
        this.number = _number;
        this.buffer = _buffer;
        this.buffer_size = _buffer_size;
    }

    @Override
    public void run() {
        for (int i = 2; i <= number; i += 2) {
            if (gen.is_prime(i)) {

                buffer.set_largest_prime(i);
                buffer.increment_number_of_primes();

                synchronized (buffer) {
                    if (buffer.size() == buffer_size) {
                        try {
                            buffer.wait();
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    buffer.add(i);
                    buffer.notify();
                }

            }
            if (i == 2) i--;
        }
        synchronized (buffer) {
            buffer.add(-1);
            buffer.notify();
        }
    }
}
