import java.util.Queue;

public class PrimesGenerator {
    public PrimesGenerator() {

    }

    private long binpower(long base, int e, int mod) {
        long result = 1;
        base %= mod;
        while (e > 0) {
            if (e % 2 == 1)
                result = result * base % mod;
            base = base * base % mod;
            e >>= 1;
        }
        return result;
    }

    private boolean check_composite(int n, long a, int d, int s) {
        long x = binpower(a, d, n);
        if (x == 1 || x == n - 1)
            return false;
        for (int r = 1; r < s; r++) {
            x = x * x % n;
            if (x == n - 1)
                return false;
        }
        return true;
    }

    boolean is_prime(int n) { // returns true if n is prime, else returns false.
        if (n < 2)
            return false;

        int r = 0;
        int d = n - 1;
        while (d % 2 == 0) {
            d >>= 1;
            r++;
        }

        for (int a : new int[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37}) {
            if (n == a)
                return true;
            if (check_composite(n, a, d, r))
                return false;
        }
        return true;
    }

    boolean is_prime2(int n) {
        if (n < 2) return false;

        for (long i = 2; i * i <= n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
