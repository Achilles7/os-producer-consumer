import java.util.LinkedList;
import java.util.Queue;

public class Buffer {
    private final Queue<Integer> queue;
    public final int BUFFERING_SIZE;

    private int largest_prime = 0, number_of_primes = 0;

    public Buffer(int bufferSize) {
        queue = new LinkedList<>();
        BUFFERING_SIZE = bufferSize;
    }

    boolean isEmpty() {
        return queue.isEmpty();
    }

    int front() {
        return queue.peek();
    }

    void add(int x) {
        queue.add(x);
    }

    int poll(){
        return queue.poll();
    }

    boolean isFull() {
        return queue.size() == BUFFERING_SIZE;
    }

    int size() {
        return queue.size();
    }

    public void set_largest_prime(int new_largest_prime) {
        largest_prime = new_largest_prime;
    }

    public void increment_number_of_primes() {
        number_of_primes++;
    }

    public int get_largest_prime() {
        return largest_prime;
    }

    public int get_number_of_primes() {
        return number_of_primes;
    }
}
